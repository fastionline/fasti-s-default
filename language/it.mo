��    	      d      �       �      �      �                *     A     R     e  Q  q  :   �     �  2     S   ?  
   �     �     �     �                         	                     fasti.aiac.title fasti.conservation.label fasti.credits fasti.csai.title fasti.excavation.label fasti.home.label fasti.survey.label fasti.title Last-Translator: John Layt, 2024
Language-Team: Italian (https://app.transifex.com/fastionline/teams/193090/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
X-Language: en_US
X-Source-Language: C
 Associazione Internazionale di Archeologia Classica (AIAC) Conservazione Fasti Online è un progetto dell' %1$s e del %2$s. Centro per lo studio dell'Italia antica dell'Università del Texas ad Austin (CSAI) Excavation Home Survey Fasti Online 